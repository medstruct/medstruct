# MEDSTRUCT

Extract structure from medical narrative (Clinical NLP).

The documentation of microservice configuration can be found [here](https://github.com/putssander/medstruct-config).

## 1. Functionalities

This repository contains:
 - History of the initial single application implementation.
 - Preprocessing
 - Sectionizer
 - Classify stream: nlp streaming pipeline for tnm-lung
 - Classify excel scripts
 
## 2. Configuration
 
### 2.1 Excel columns  
Separate columns for tnm can be configured, when a value for separate columns is missing the label-tnm-column is used (which should contain the full tnm).
 
     medstruct:
       lang: 'en'
       xlsx:
         text-column: 'report'
         label-tnm-column:
         label-t-column: 'LABEL_T'
         label-n-column: 'LABEL_N'
         label-m-column: 'LABEL_M'

         
### 2.2 Sectionizer

#### 2.2.1 report style

The sectionizer can currently handle 2 report styles: mumc, boston
- mumc: a new section starts with a heading, sections are split using white lines
- boston: specific space indents are used (not likely to be reusable) 

#### 2.2.2 regex:

required, optional and blacklisted regex headings
- whitelist_required: when detected only this section and optional sections are processed. Be careful! Use a regex for a single section, all sections listed are required.
- whitelist_optional: when required section(s) detected, only optional and required sections are processed
- blacklist: these sections are never processed


    simple-sectionizer:
      regex_whitelist_required:
        - 'thorax:'
      regex_whitelist_optional:
        - 'conclusie:'
        - 'conclusie'
      regex_blacklist:
        - 'gegevens:'
        - 'vraagstelling:'
      report_style: 'mumc'
      enabled: true

### 2.3 Pre-processing
Replace abbreviations and punctuations.      
       
       preprocessor:
         replace-abbreviations:
           "ca.": "circa"
         replace-punctuations:
           ":": ""
           "-": " "