from medstruct.stats.ComputeMetrics import ComputeMetrics


class WriteResultsTnm(object):
    
    results = {}

    def __init__(self, df, cfg, output_dir, results_title):
        
        compute = ComputeMetrics(
            df,
            output_dir=output_dir,
            file_name_prefix=cfg['medstruct']['results']['confusion-matrix-prefix'],
            image_title=results_title,
            file_type_eps=cfg['medstruct']['results']['confusion-matrix-eps'])

        df.head()
        df.to_excel(output_dir + "/results.xlsx")
        self.results = compute.results