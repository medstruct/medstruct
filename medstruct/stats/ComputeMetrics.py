import json
import pandas as panda
import matplotlib.pyplot as plt
import numpy as numpy
import time
from sklearn.metrics import precision_recall_fscore_support, confusion_matrix


class ComputeMetrics(object):
    
    results = {}        

    def __init__(self, result_df, output_dir, file_name_prefix, image_title, file_type_eps):

        print("Computing evaluation metrics...")
        acc_df = result_df[result_df["label_T"] != "?"] # Filter dataframe on reports that have a valid label

        print("")
        print("ACCURACY RESULTS FROM VARIOUS METHODS:")
        print("-----------------------------------------------")
        acc_naive = acc_df[acc_df["label_T"] == acc_df["naive_T_class_simple"]].shape[0]/acc_df.shape[0] # Compute accuracy
        print("Naive classification (only tumor measurements):")
        print("Accuracy = " + str(round(acc_naive*100, 2)) + "%") # Print result in percentages
        print("-----------------------------------------------")

        # acc_t4 = acc_df[acc_df["T4_naive_class"] == acc_df["label_T"]].shape[0]/acc_df.shape[0] # Compute accuracy
        # print("Classification using additional T4 rules:")
        # print("Accuracy = " + str(round(acc_t4*100, 2)) + "%") # Print result in percentages
        # print("-----------------------------------------------")
        # acc_t3 = acc_df[acc_df["T3_naive_class"] == acc_df["label_T"]].shape[0]/acc_df.shape[0] # Compute accuracy
        # print("Classification using additional T3 rules:")
        # print("Accuracy = " + str(round(acc_t3*100, 2)) + "%") # Print result in percentages
        # print("-----------------------------------------------")
        acc_t4_t3 = acc_df[acc_df["class_T"] == acc_df["label_T"]].shape[0]/acc_df.shape[0] # Compute accuracy
        print("Classification using additional T4 & T3 rules:")
        print("Accuracy = " + str(round(acc_t4_t3*100, 2)) + "%") # Print result in percentages
        print("-----------------------------------------------")
        print("")

        acc_t_full = acc_df[result_df["class_T_complete"] == result_df["label_T_complete"]].shape[0]/acc_df.shape[0] # Compute accuracy
        print("Classification class_T_complete:")
        print("Accuracy = " + str(round(acc_t_full*100, 2)) + "%") # Print result in percentages
        print("-----------------------------------------------")
        print("")

        y_actu = panda.Series(acc_df['label_T'], name='Actual')
        y_pred = panda.Series(acc_df['naive_T_class_simple'], name='Predicted')
        df_confusion = panda.crosstab(y_actu, y_pred, rownames=['Actual'], colnames=['Predicted'], margins=True)
        print("CONFUSION MATRIX OF CLASSIFICATION USING ADDITIONAL naive_T_class_simple:")
        print("------------------------------------------------------------------")
        print("")
        print(df_confusion)

        y_actu = panda.Series(acc_df['label_T'], name='Actual')
        y_pred = panda.Series(acc_df['class_T'], name='Predicted')
        df_confusion = panda.crosstab(y_actu, y_pred, rownames=['Actual'], colnames=['Predicted'], margins=True)
        print("CONFUSION MATRIX OF CLASSIFICATION USING ADDITIONAL T4 & T3 RULES:")
        print("------------------------------------------------------------------")
        print("")
        print(df_confusion)
        print("")
        class_names = ['T0', 'T1', 'T2', 'T3', 'T4']
        prf = precision_recall_fscore_support(y_actu, y_pred, average=None, labels=class_names)

        evaldf = panda.DataFrame({"Precision": prf[0], "Recall": prf[1], "F-score": prf[2]}, index=class_names)
        print(evaldf)
        print("-------------------------------------------------------------------")

        numpy.set_printoptions(precision=2)


        # Plot non-normalized confusion matrix
        actual_values = []
        for v in y_actu.values:
            actual_values.append(class_names.index(v))

        pred_values = []
        for v in y_pred.values:
            pred_values.append(class_names.index(v))

        plot_confusion_matrix(actual_values, pred_values, classes=class_names,
                              title=image_title + ' (N='+str(len(actual_values))+")")
        output_path = output_dir + '/' + file_name_prefix

        if file_type_eps:
            plt.savefig(output_path + ".eps", format="eps")
        else:
            plt.savefig(output_path + ".png")

        print("-----------------------------------------------")
        print("Classification N-rules:")
        acc_df_n = result_df[result_df["label_N"] != "?"] # Filter dataframe on reports that have a valid label
        if not acc_df_n.shape[0]:
            print("no N")
            return
        acc_n = acc_df_n[acc_df_n["class_N"] == acc_df_n["label_N"]].shape[0]/acc_df_n.shape[0] # Compute accuracy

        print("Accuracy = " + str(round(acc_n*100, 2)) + "%") # Print result in percentages
        print("-----------------------------------------------")
        print("")
        class_names = ['N0', 'N1', 'N2', 'N3']
        y_actu = panda.Series(acc_df_n['label_N'], name='Actual')
        y_pred = panda.Series(acc_df_n['class_N'], name='Predicted')
        df_confusion = panda.crosstab(y_actu, y_pred, rownames=['Actual'], colnames=['Predicted'], margins=True)
        print("CONFUSION MATRIX OF CLASSIFICATION N-STAGE")
        print("------------------------------------------------------------------")
        print("")
        print(df_confusion)

        prf = precision_recall_fscore_support(y_actu, y_pred, average=None, labels=class_names)

        evaldf = panda.DataFrame({"Precision": prf[0], "Recall": prf[1], "F-score": prf[2]}, index=class_names)
        print(evaldf)
        print("-------------------------------------------------------------------")

        acc_df_tn = acc_df[acc_df["label_N"] != "?"] # Filter dataframe on reports that have a valid label
        acc_tn = acc_df_tn[acc_df_tn["class_TN"] == acc_df_tn["label_TN"]].shape[0]/acc_df_tn.shape[0] # Compute accuracy

        print("Accuracy TN = " + str(round(acc_tn*100, 2)) + "%") # Print result in percentages
        print("TN count")
        print(acc_df_tn["label_TN_correct"].value_counts()) # Print result in percentages

        self.results["title"] = image_title
        self.results["T"] = acc_t_full
        self.results["N"] = acc_n
        self.results["TN"] = acc_tn


def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    labels = []
    for i in range(len(classes)):
        labels.append(i)

    cm = confusion_matrix(y_true, y_pred, labels=labels)

    # Only use the labels that appear in the data
    # classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, numpy.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=numpy.arange(cm.shape[1]),
           yticks=numpy.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax

