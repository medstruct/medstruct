import pandas as panda
import matplotlib.pyplot as plt
import numpy as numpy
from sklearn.metrics import precision_recall_fscore_support, confusion_matrix
from sklearn.utils.multiclass import unique_labels


class ComputeMetricsGeneric(object):

    def __init__(self, df, y_actu_column, y_pred_column, output_dir, file_name_prefix, image_title, file_type_eps, slice):

        print("Computing evaluation metrics...")
        df = df[df[y_actu_column] != "?"] # Filter dataframe on reports that have a valid label
        df[y_actu_column] = df[y_actu_column].apply(lambda x: x.strip())
        if slice:
            df[y_actu_column] = df[y_actu_column].apply(lambda x: x[:2])
            df[y_pred_column] = df[y_pred_column].apply(lambda x: x[:2])

        print("")
        print("ACCURACY:")
        accuracy = df[df[y_pred_column] == df[y_actu_column]].shape[0]/df.shape[0] # Compute accuracy
        print("Classification of {} and {}", y_actu_column, y_pred_column)
        print("Accuracy = " + str(round(accuracy*100, 2)) + "%") # Print result in percentages
        print("-----------------------------------------------")
        print("")

        y_actu = panda.Series(df[y_actu_column], name='Actual')
        y_pred = panda.Series(df[y_pred_column], name='Predicted')
        df_confusion = panda.crosstab(y_actu, y_pred, rownames=['Actual'], colnames=['Predicted'], margins=True)
        print("CONFUSION MATRIX")
        print("------------------------------------------------------------------")
        print("")
        print(df_confusion)

        labels = unique_labels(y_actu, y_pred)
        prf = precision_recall_fscore_support(y_actu, y_pred, average=None, labels=labels)
        evaldf = panda.DataFrame({"Precision": prf[0], "Recall": prf[1], "F-score": prf[2]}, index=labels)
        print(evaldf)
        print("-------------------------------------------------------------------")

        numpy.set_printoptions(precision=2)
        plot_confusion_matrix(y_actu, y_pred, title=image_title + ' (N='+str(len(y_pred))+")")
        output_path = output_dir + '/' + file_name_prefix

        if file_type_eps:
            plt.savefig(output_path + ".eps", dict="eps")
        else:
            plt.savefig(output_path + ".png")


def function_strip(s):
    return s.strip()

def function_substring(s):
    return s.strip()

def plot_confusion_matrix(y_true, y_pred, normalize=False, title=None, cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    labels = unique_labels(y_true, y_pred)
    cm = confusion_matrix(y_true, y_pred, labels=labels)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, numpy.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=numpy.arange(cm.shape[1]),
           yticks=numpy.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=labels, yticklabels=labels,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax

