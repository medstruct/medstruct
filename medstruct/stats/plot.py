import yaml
import sys
import pandas as panda
import os
import time
import json

from medstruct.classify.lung.classify_t_stream import ReportT
from medstruct.stats.ComputeMetrics import ComputeMetrics
from medstruct.stats.ComputeMetricsGeneric import ComputeMetricsGeneric

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
CONFIG_PATH = os.path.join(ROOT_DIR, '../../application.yml')

with open(CONFIG_PATH, 'r') as config_file:
    cfg = yaml.safe_load(config_file)


class Compute(object):

    # def __init__(self):

    def plot(self, filename_in, y_actu_column, y_pred_column, results_title, slice):
        results_title_time = results_title + '-' + time.strftime("%Y%m%d-%H%M%S")

        output_dir = cfg['medstruct']['results']['output-dir']
        output_dir = output_dir + '/' + results_title_time
        os.makedirs(output_dir, mode=0o777, exist_ok=False)

        # report_t = ReportT(CONFIG_PATH)
        df = panda.read_excel(filename_in, header=0)

        compute = ComputeMetricsGeneric(
            df,
            y_actu_column, y_pred_column,
            output_dir=output_dir,
            file_name_prefix=cfg['medstruct']['results']['confusion-matrix-prefix'],
            image_title=results_title,
            file_type_eps=cfg['medstruct']['results']['confusion-matrix-eps'],
            slice=slice
        )

        return

compute = Compute()
# filename_in = '/Users/sanderputs/DataSets/Research/MEE/Last and final test set 200 (03-10-2019)/bostontest200.xlsx'
# filename_in = '/Users/sanderputs/DataSets/Research/MEE/Last and final validation set 200 (03-10-2019)/Last and final validation set CLEAN 200 (03-10-2019).xlsx'
# filename_in = '/Users/sanderputs/DataSets/Research/MEE/Last and final validation set 225 (03-10-2019)/Last and final validation set 225 CLEAN (03-10-2019).xlsx'
filename_in = '/Users/sanderputs/Data/Result/validate-tn-20200702-113221/results.xlsx'

y_actu_column = 'GOLD-T'
y_pred_column = 'class_T_complete'
results_title = 'Confusion Matrix (validation-set)'

compute.plot(filename_in, y_actu_column, y_pred_column, results_title, True)
compute.plot(filename_in, y_actu_column, y_pred_column, results_title, False)
