#!/usr/bin/python
# coding: utf-8

import yaml
import pandas as panda
import os
import time
import json
import argparse

from medstruct.classify.lung.classify_t_stream import ReportT
from medstruct.stats.write_results_tnm import WriteResultsTnm

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

class XlsxRun(object):

    config_path = os.path.join(ROOT_DIR, '../application.yml')
    cfg = None

    def classify(self, filename_in, results_title, config_path_arg=None):

        if config_path_arg:
            self.config_path = config_path_arg

        with open(self.config_path, 'r') as config_file:
            self.cfg = yaml.safe_load(config_file)

        results_title_time = results_title + '-' + time.strftime("%Y%m%d-%H%M%S")
        output_dir = self.cfg['medstruct']['results']['output-dir']
        output_dir = output_dir + '/' + results_title_time
        os.makedirs(output_dir, mode=0o777, exist_ok=False)

        df = self.classify_stream(filename_in, output_dir)

        write_results = WriteResultsTnm(df, self.cfg, output_dir, results_title)
        return write_results.results


    def classify_stream(self, xlsx_reports, output_dir):

        report_t = ReportT(self.config_path)
        print("--------------------------------")
        print("PARSING XLSX FILE=", xlsx_reports)

        df = panda.read_excel(xlsx_reports, header=0)

        label_tnm = self.cfg['medstruct']['xlsx']['label-tnm-column']
        label_t = self.cfg['medstruct']['xlsx']['label-t-column']
        label_n = self.cfg['medstruct']['xlsx']['label-n-column']
        label_m = self.cfg['medstruct']['xlsx']['label-m-column']

        if label_t and label_n and label_m:
            print("Using seperate TNM columns for labeling")
            df['label_T'] = df[label_t].apply(ReportT.get_t_score_label)
            df['label_T_complete'] = df[label_t].apply(ReportT.get_t_score_label_complete)
            df['label_N'] = df[label_n].apply(ReportT.get_n_score_label)
            df['label_M'] = df[label_m].apply(ReportT.get_m_score_label)
        elif label_tnm:
            print("Using single TNM column for labeling")
            df['label'] = df[label_tnm]
            df['label_T'] = df[label_tnm].apply(ReportT.get_t_score_label)
            df['label_T_complete'] = df[label_tnm].apply(ReportT.get_t_score_label_complete)
            df['label_N'] = df[label_tnm].apply(ReportT.get_n_score_label)
            df['label_M'] = df[label_tnm].apply(ReportT.get_m_score_label)
        else:
            print("Error labeling not correct configured!")

        result_columns = self.cfg['tnm-classification']['xlsx-result-columns']

        for i, row in df.iterrows():

            raw_text = row[self.cfg['medstruct']['xlsx']['text-column']]
            jsonnlp_tnm = report_t.classify(i, raw_text)
            data = report_t.jsonnlp_tnm_flat(jsonnlp_tnm)
            for k, v in data.items():
                if k in result_columns:
                    if v is None:
                        df.at[i, k] = v
                    else:
                        df.at[i, k] = json.dumps(v)

            df.at[i, 'naive_T_class_simple'] = data['naive_T_class_simple']
            df.at[i, 'tumor-size'] = data['tumor-size']
            df.at[i, 'class_T'] = data['class_T']
            df.at[i, 'class_T_complete'] = data['class_T_complete']
            df.at[i, 'class_N'] = data['class_N']
            df.at[i, 'class_M'] = data['class_M']

            df.at[i, 'label_T_correct'] = ReportT.label_correct(data['class_T'], row['label_T'])
            df.at[i, 'label_T_complete_correct'] = ReportT.label_correct(data['class_T_complete'], row['label_T_complete'])

            df.at[i, 'label_N_correct'] = ReportT.label_correct(data['class_N'], row['label_N'])

            df.at[i, 'label_TN'] = ReportT.column_tn_merge(row['label_T_complete'], row['label_N'])
            df.at[i, 'class_TN'] = ReportT.column_tn_merge(data['class_T_complete'], data['class_N'])
            df.at[i, 'label_TN_correct'] = ReportT.label_correct(df.at[i, 'class_TN'], df.at[i, 'label_TN'])

            if self.cfg['medstruct']['results']['dump-json-nlp']:
                with open(output_dir + '/' + str(i) + '.json', 'w') as outfile:
                            json.dump(jsonnlp_tnm, outfile, indent=4)

        return df



def parseArguments():
    # Create argument parser
    parser = argparse.ArgumentParser()

    # Positional mandatory arguments
    parser.add_argument("filenameIn", help="xlsx file, configure columns in application.yml")
    parser.add_argument("resultsTitle", help="title for the graph")

    # Print version
    parser.add_argument("--version", action="version", version='%(prog)s - Version 2.2')

    # Parse arguments
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    # Parse the arguments
    args = parseArguments()

    # Raw print arguments
    print("You are running the script with arguments: ")
    for a in args.__dict__:
        print(str(a) + ": " + str(args.__dict__[a]))

    # if filename_in:
    xlsx = XlsxRun()
    xlsx.classify(args.filenameIn, args.resultsTitle)



