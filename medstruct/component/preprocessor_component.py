import regex as re
import yaml


class PreprocessorComponent(object):

    def __init__(self, config_file):
        with open(config_file, 'r') as ymlfile:
            cfg = yaml.safe_load(ymlfile)
        print(cfg)

        self.abbreviations = cfg['preprocessor']['replace-abbreviations']
        self.punctuations = cfg['preprocessor']['replace-punctuations']


    @staticmethod
    def preprocess_raw_numbers_dictation_artifacts_correction(text):
        """
        Clean numbers from dictation artifacts where white spaces appear within numbers
        :param doc: text
        :return: text where whitespaces are removed if they follow the pattern number-dot-white_space_number
        """
        text = re.sub(r'([0-9])\. ([0-9])', r'\1.\2', text) #remove whitespace within numbers
        text = re.sub(r'([0-9]+[-][0-9]+)\.([0-9]+)', r'\1 is \2', text) #norm ima
        text = re.sub(r'([0-9])\. ([0-9])', r'\1.\2', text)
        # text = re.sub(r'([0-9]), ([0-9])', r'\1,\2', text) //find out why this does not work for NL set
        text = re.sub(r'([0-9])([A-Za-z])', r'\1 \2', text) #add whitespace between numbers and letters
        text = re.sub(r'([A-Za-z])([0-9])', r'\1 \2', text) #add whitespace between numbers and letters


        return text

    def preprocess_raw_abbreviations(self, text):
        """
        Map abbreviations targeting numbers to its full form
        :param text including abbrevations
        :return: text including full form of selected abbreviations
        """
        for key in self.abbreviations:
            text = text.replace(key, self.abbreviations[key])
        return text

    def preprocess_punctuation(self, text):
        """
        Replace punctations to improve sentence splitter
        :param text
        :return: text cleaned with replaced punctations
        """
        for key in self.punctuations:
            text = text.replace(key, self.punctuations[key])
        return text