import regex as re
import numpy as np

from itertools import chain


class SectionizerComponent(object):

    @staticmethod
    def get_filtered_sections(text, regex_whitelist_required, regex_whitelist_optional,
                              regex_blacklist,
                              report_style):
        if report_style == 'boston':
            raw_sections = SectionizerComponent.get_raw_sections_boston_style(text)
        else:
            raw_sections = SectionizerComponent.get_raw_sections_mumc_style(text)
        required_sections = SectionizerComponent.get_sections(raw_sections, regex_whitelist_required)
        optional_sections = SectionizerComponent.get_sections(raw_sections, regex_whitelist_optional)

        if len(required_sections) >= len(regex_whitelist_required):
            print("sectionizer whitelist applied")
            section_whitelist = required_sections
            if len(optional_sections) > 0:
                section_whitelist = section_whitelist + optional_sections

            np_raw_sections = np.array(raw_sections, dtype=object)
            sections = list(np_raw_sections[section_whitelist])
            sections = "\n".join(list(chain.from_iterable(sections)))
            return sections
        else:
            section_blackist = SectionizerComponent.get_sections(raw_sections, regex_blacklist)
            if len(section_blackist) > 0:
                print("sectionizer blacklist applied")
                raw_sections = [section for idx, section in enumerate(raw_sections) if idx not in section_blackist]
            else:
                print("sectionizer not applied")
            sections = "\n".join(list(chain.from_iterable(raw_sections)))
            return sections

    @staticmethod
    def get_raw_sections_mumc_style(text):
        split_list = text.splitlines()
        for split in split_list:
            split.rstrip()

        end = len(split_list)-1
        sections = list()
        current_section = list()
        for idx, split in enumerate(split_list):
            if idx == end:
                current_section.append(split)
                sections.append(current_section)
                current_section = list()
            elif len(split) == 0:
                sections.append(current_section)
                current_section = list()
            else:
                current_section.append(split)

        return sections

    @staticmethod
    def get_raw_sections_boston_style(text):
        split_list = str(text).split('     ')
        for split in split_list:
            split.rstrip()

        end = len(split_list)-1
        sections = list()
        current_section = list()
        for idx, split in enumerate(split_list):
            if idx == end:
                current_section.append(split)
                sections.append(current_section)
                current_section = list()
            elif len(split) == 0 or split == ' \n' and ":" in split_list[idx+1]:
                sections.append(current_section)
                current_section = list()
            else:
                current_section.append(split)
        print('found sections=', len(sections))
        return sections

    @staticmethod
    def get_sections(sections, regex_title):

        filtered_section_list = list()

        append = False
        for idx, section in enumerate(sections):
            if len(section) > 0:
                first_section_line = section[0]

                append_new = False
                for regex in regex_title:
                    if re.search(r'('+regex+')', first_section_line, re.IGNORECASE):
                        filtered_section_list.append(idx)
                        append_new = True
                        break

                first_section_line_heading_colon = re.search(r'('+':'+')', first_section_line, re.IGNORECASE) is not None
                first_section_line_heading_no_colon = len(first_section_line.split()) < 3

                if append and not (append_new or first_section_line_heading_colon or first_section_line_heading_no_colon):
                    filtered_section_list.append(idx)
                    append_new = True

                if not append_new:
                    append = False
                else:
                    append = True

        return filtered_section_list
