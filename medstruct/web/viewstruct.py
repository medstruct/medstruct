from pathlib import Path

from spacy import displacy
import nl_core_news_sm

nlp = nl_core_news_sm.load()
#
# def displacy_service_ents(text):
#     doc = nlp(text)
#     return displacy.parse_ents(doc)

import spacy
from spacy import displacy

text = """
Clinical details:
Pulmonary malignancy?

Report:
CT thorax and abdomen, arterial phase

Thorax:
Mass visible in the left upper lobe with a maximum size estimated at 8-46 of 4, 7 x 3,0 cm. Possible involvement in mediastinum. Satellite nodes visible at 8-41 with an estimated size of 1.3 cm. Lymph node visible at station 7 with a size of circa 5,2 cm. No lymph nodes visible at contralateral side. Small consolidation middle lobe. No indication of atelectasis. 

Abdomen:
Multiple sharply edged hypodens lesions visible which would initially match with cysts (HU 5).

Musculoskeletal 
No relevant findings. No metastasis.

Conclusion:
Tumor with satellite nodes left upper lobe

"""

ents = [
    {"start": 87, "end": 95, "label": "HEADING"},
    {"start": 116, "end": 13, "label": "TUMOR"},
    # {"start": 39, "end": 59, "label": "T4-PRESENCE"},
    # {"start": 105, "end": 117, "label": "SIZE"},
    # {"start": 119, "end": 135, "label": "T3-PRESENCE"},
    # {"start": 178, "end": 184, "label": "SIZE"},
    # {"start": 185, "end": 194, "label": "CONTEXT"},
    # {"start": 194, "end": 206, "label": "INVOLVEMENT"},
    # {"start": 210, "end": 221, "label": "T2-PRESENCE"},
    # {"start": 223, "end": 234, "label": "LYMPH"},
    # {"start": 276, "end": 282, "label": "SIZE"},
    # {"start": 284, "end": 286, "label": "CONTEXT"},
    # {"start": 287, "end": 298, "label": "LYMPH"},
    # {"start": 330, "end": 344, "label": "CONTEXT"},
    # {"start": 347, "end": 358, "label": "T2-PRESENCE"},
    # {"start": 360, "end": 373, "label": "HEADING"}
]

# nlp = spacy.load("custom_ner_model")
doc = nlp(text)
sentence_spans = list(doc.sents)

# displacy.serve(doc, style="ent")

ex = [{"text": text,
       "ents": ents,
       "title": None}]

options = {"colors": {"HEADING": "#ffa3a3",
         "SIZE": "#ffc8a2",
         "CONTEXT": "#fff7a6",
         "TUMOR": "#b6ffa6",
         }}
# svg = displacy.render(ex, style="ent", manual=True)
# output_path = Path("./report-annotated.html")
# output_path.open("w", encoding="utf-8").write(svg)
html = displacy.serve(ex, style="ent", manual=True, options=options)

