#!/opt/conda/bin/python
# -*- coding: utf-8 -*-

"""

"""
import os
import logging
import yaml
from flask import Flask
from flask import request
from flask import jsonify

from medstruct.classify.lung.classify_t_stream import ReportT

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%a, %d %b %Y %H:%M:%S',
                    handlers=[logging.FileHandler("../../server.log"),
                              logging.StreamHandler()])


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
CONFIG_PATH = os.path.join(ROOT_DIR, '../../application.yml')

with open(CONFIG_PATH, 'r') as config_file:
    cfg = yaml.safe_load(config_file)
report_t = ReportT(CONFIG_PATH)


app = Flask(__name__)
app.config["JSON_SORT_KEYS"] = False


@app.route('/api/tnm/lung-8', methods=['POST'])
def process_flask():
    content = request.get_json(silent=True)
    raw_text = content['text']

    i = '-1'
    lang = None
    method = None
    if 'identifier' in content:
        i = content['identifier']
    if 'lang' in content:
        lang = content['lang']
    if 'method' in content:
        method = content['method']

    logging.info('processing START identifier=%s', i)
    jsonnlp_tnm = report_t.classify(i, raw_text, lang=lang, method=method)
    logging.info('processing DONE identifier=%s', i)
    return jsonify(jsonnlp_tnm)


if __name__ == "__main__":
    app.run(debug=False, port=8081, host='0.0.0.0')
