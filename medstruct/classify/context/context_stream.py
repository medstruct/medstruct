#!/usr/bin/env python
import re
import yaml

from medstruct.client.medstruct_measurementextractor_jsonnlp_client import MedstructMeasurementClient
from medstruct.client.medstruct_tnm_client import MedstructTnmClient
from medstruct.client.pycontextnlp_jsonnlp_client import PyContextNlpClient
from medstruct.client.spacy_jsonnlp_client import SpacyClient

from medstruct.component.preprocessor_component import PreprocessorComponent
from medstruct.component.sectionizer_component import SectionizerComponent


class ContextStream(object):

    def __init__(self, config_file):
        with open(config_file, 'r') as ymlfile:
            cfg = yaml.safe_load(ymlfile)
            print(cfg)

            self.preprocessor = PreprocessorComponent(config_file)
            self.spacy_client = SpacyClient(cfg['spacy']['url'], cfg['spacy']['models'])
            self.pycontextnlp_client = PyContextNlpClient(cfg['pycontextnlp']['url-lang-map'])
            self.default_lang = cfg['medstruct']['lang']

            self.spacy_client = SpacyClient(cfg['spacy']['url'], cfg['spacy']['models'])
            self.pycontextnlp_client = PyContextNlpClient(cfg['pycontextnlp']['url-lang-map'])
            self.medstruct_measurement_client = MedstructMeasurementClient(cfg['medstruct-measurement-extractor']['url'])
            self.medstruct_tnm_client = MedstructTnmClient(cfg['medstruct-tnm-client']['url'])

            self.image_title = "Confusion matrix, validation set"

            self.report_column = cfg['medstruct']['xlsx']['text-column']
            self.default_lang = cfg['medstruct']['lang']

            self.regex_whitelist_required = cfg['simple-sectionizer']['regex_whitelist_required']
            self.regex_whitelist_optional = cfg['simple-sectionizer']['regex_whitelist_optional']
            self.regex_blacklist = cfg['simple-sectionizer']['regex_blacklist']
            self.sectionizer_report_style = cfg['simple-sectionizer']['report_style']
            self.sectionizer_enabled = cfg['simple-sectionizer']['enabled']

            if not self.sectionizer_enabled:
                print("SECTIONIZER DISABLED")


    def get_context(self, identifier, raw_text, lang=None):
        print("----------------------------------------")
        print("Classify Context ", identifier)
        raw_text = str(raw_text)

        if lang is None:
            lang = self.default_lang

        if self.sectionizer_enabled:
            raw_text = SectionizerComponent \
                .get_filtered_sections(raw_text, self.regex_whitelist_required, self.regex_whitelist_optional, self.regex_blacklist, self.sectionizer_report_style)

        cleaned_report = self.clean_text(raw_text)

        jsonnlp = self.spacy_client.annotate(text=cleaned_report, identifier=identifier, document_date=None, lang=lang)
        jsonnlp_context = self.pycontextnlp_client.annotate(jsonnlp=jsonnlp, lang=lang)

        return jsonnlp_context

    def clean_text(self, text):
        text = self.preprocessor.preprocess_punctuation(text)
        return text
