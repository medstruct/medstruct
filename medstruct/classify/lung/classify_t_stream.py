#!/usr/bin/env python
import re
import yaml

from medstruct.client.medstruct_measurementextractor_jsonnlp_client import MedstructMeasurementClient
from medstruct.client.medstruct_tnm_client import MedstructTnmClient
from medstruct.client.pycontextnlp_jsonnlp_client import PyContextNlpClient
from medstruct.client.spacy_jsonnlp_client import SpacyClient

from medstruct.component.preprocessor_component import PreprocessorComponent
from medstruct.component.sectionizer_component import SectionizerComponent


class ReportT(object):

    def __init__(self, config_file):

        with open(config_file, 'r') as ymlfile:
            cfg = yaml.safe_load(ymlfile)
        print(cfg)

        self.preprocessor = PreprocessorComponent(config_file)

        self.spacy_client = SpacyClient(cfg['spacy']['url'], cfg['spacy']['models'])
        self.pycontextnlp_client = PyContextNlpClient(cfg['pycontextnlp']['url-lang-map'])
        self.medstruct_measurement_client = MedstructMeasurementClient(cfg['medstruct-measurement-extractor']['url'])
        self.medstruct_tnm_client = MedstructTnmClient(cfg['medstruct-tnm-client']['url'])

        self.image_title = "Confusion matrix, validation set"

        self.report_column = cfg['medstruct']['xlsx']['text-column']
        self.default_lang = cfg['medstruct']['lang']

        self.regex_whitelist_required = cfg['simple-sectionizer']['regex_whitelist_required']
        self.regex_whitelist_optional = cfg['simple-sectionizer']['regex_whitelist_optional']
        self.regex_blacklist = cfg['simple-sectionizer']['regex_blacklist']
        self.sectionizer_report_style = cfg['simple-sectionizer']['report_style']
        self.sectionizer_enabled = cfg['simple-sectionizer']['enabled']

        if not self.sectionizer_enabled:
            print("SECTIONIZER DISABLED")

    def classify(self, identifier, raw_text, lang=None, method=None):
        print("----------------------------------------")
        print("CLASSIFY T-STAGE identifier ", identifier)
        raw_text = str(raw_text)

        if lang is None:
            lang = self.default_lang

        if self.sectionizer_enabled:
                raw_text = SectionizerComponent\
                .get_filtered_sections(raw_text, self.regex_whitelist_required, self.regex_whitelist_optional, self.regex_blacklist, self.sectionizer_report_style)

        cleaned_report = self.clean_text(raw_text)

        # spacy_doc = self.run_spacy(cleaned_report)
        jsonnlp = self.spacy_client.annotate(text=cleaned_report, identifier=identifier, document_date=None, lang=lang)
        jsonnlp_context = self.pycontextnlp_client.annotate(jsonnlp=jsonnlp, lang=lang)
        jsonnlp_context_measurements = self.medstruct_measurement_client.get_measurements(jsonnlp_context)
        jsonnlp_tnm = self.medstruct_tnm_client.get_tnm(jsonnlp_context_measurements)
        return jsonnlp_tnm

    def clean_text(self, text):
        text = PreprocessorComponent.preprocess_raw_numbers_dictation_artifacts_correction(text)
        text = self.preprocessor.preprocess_raw_abbreviations(text)
        text = self.preprocessor.preprocess_punctuation(text)
        return text

    @staticmethod
    def jsonnlp_tnm_flat(jsonnlp_tnm):
        data = {}
        data['class_T'] = 'T0'
        data['naive_T_class_simple'] = 'T0'
        data['class_T_complete'] = 'T0'
        data['class_N'] = 'N0'
        data['class_M'] = '?'
        data['tumor-size'] = '-1'
        try:

            for k, v in jsonnlp_tnm['documents'][0]['tnm-classification'].items():
                data[k] = v
            if jsonnlp_tnm['documents'][0]['tnm-classification']['tnm']:
                data['class_T'] = ReportT.get_t_score_label(jsonnlp_tnm['documents'][0]['tnm-classification']['tnm'])
                data['class_T_complete'] = ReportT.get_t_score_label_complete(jsonnlp_tnm['documents'][0]['tnm-classification']['tnm'])
                data['class_N'] = ReportT.get_n_score_label(jsonnlp_tnm['documents'][0]['tnm-classification']['tnm'])
                data['class_M'] = ReportT.get_m_score_label(jsonnlp_tnm['documents'][0]['tnm-classification']['tnm'])

            data['naive_T_class_simple'] = ReportT.get_t_score_label(jsonnlp_tnm['documents'][0]['tnm-classification']['tnmSize'])
            data['tumor-size'] = jsonnlp_tnm['documents'][0]['tnm-classification']['tumorSize']
        except TypeError as e:
            print(e)
        except KeyError as e:
            print(e)

        return data

    # Extract first two characters from TNM code
    @staticmethod
    def get_t_score_label(label):
        if not label:
            return "?"
        label = str(label)
        matches = ["T0", "T1", "T2", "T3", "T4"]
        for i in matches:
            if re.search(i, label, re.IGNORECASE):
                return i
        return "?"

    @staticmethod
    def get_t_score_label_complete(label):
        if not label:
            return "?"
        label = str(label)
        matches = ["Tx", "T0", "Tis", "T1a", "T1b", "T1c", "T1", "T2a", "T2b", "T2", "T3", "T4"]
        for i in matches:
            if re.search(i, label, re.IGNORECASE):
                return i
        return "?"

    @staticmethod
    def get_n_score_label(label):
        if not label:
            return "?"
        label = str(label)
        matches = ["N0", "N1", "N2", "N3"]
        for i in matches:
            if re.search(i, label, re.IGNORECASE):
                return i
        return "?"

    @staticmethod
    def get_m_score_label(label):
        if not label:
            return "?"
        label = str(label)
        matches = ["M0", "M1a", "M1b", "M1c", "M1"]
        for i in matches:
            if re.search(i, label, re.IGNORECASE):
                return i
        return "?"

    @staticmethod
    def label_correct(classification, label):
        if label == '?':
            return '?'
        return classification == label

    @staticmethod
    def column_tn_merge(col_t, col_n):
        if col_t == '?' or col_n == '?':
            return '?'
        return col_t + col_n
