import regex as re


class ContextParser(object):

    probable_negated_existence = 'probable_negated_existence'
    definite_negated_existence = 'definite_negated_existence'
    pseudoneg = 'pseudoneg'

    probable_existence = 'probable_existence'
    indication = 'indication'

    historical = 'historical'
    definite_existence = 'definite_existence'
    ambivalent_existence = 'ambivalent_existence'

    limited_amount = 'limited_amount'

    exist = [definite_existence, historical, limited_amount]
    negated = [definite_negated_existence, pseudoneg]
    uncertain = [probable_existence, probable_negated_existence, indication, ambivalent_existence]
    exists_minus_limited_amount = [definite_existence, historical]

    @staticmethod
    def get_targets_category_filtered(pycontext_result, category):
        if not pycontext_result:
            return ""

        targets = []
        for target in pycontext_result:
            if category.lower() in target['category']:
                targets.append(target)
        return targets

    @staticmethod
    def get_modified_targets(pycontext_result):
        if not pycontext_result:
            return ""

        targets = []
        for target in pycontext_result:
            if 'modifier_category' in target:
                targets.append(target)
        return targets

    @staticmethod
    def has_context_modifier(pycontext_result, context_modifiers):
        if not pycontext_result:
            return False

        context_modifiers_regex = "|".join(context_modifiers)

        for target in ContextParser.get_modified_targets(pycontext_result):
            modifier_categories = target['modifier_category']
            for modifier_category in modifier_categories:
                if len(re.findall(r'('+context_modifiers_regex+')', modifier_category, re.IGNORECASE)) > 0:
                    return True
        return False

    @staticmethod
    def has_no_context_modifier(pycontext_result):
        if not pycontext_result:
            return False

        modifiers = ContextParser.get_modified_targets(pycontext_result)
        if modifiers:
            return False
        else:
            return True

    @staticmethod
    def create_target(direction, lex, regex, type):
        return [{'direction': direction, 'lex': lex, 'regex': regex, 'type': type}]

    @staticmethod
    def get_context_regex(doc, context_client, regex, category):
        target = ContextParser.create_target('', category, regex, category)

        results = []
        for sent in doc.sents:
            result = context_client.annotate(sent.string, target)
            result_category = ContextParser.get_targets_category_filtered(result, category)
            if len(result_category) > 0:
                results.append(result)
            else:
                results.append(None)
        return results

    @staticmethod
    def filter_context(context_result, context_modifiers_list):
        results = []
        for result in context_result:
            if not ContextParser.has_context_modifier(result, context_modifiers_list):
                results.append(result)
            else:
                results.append(None)
        return results

    @staticmethod
    def filter_context_no_modifier(context_result):
        results = []
        for result in context_result:
            if ContextParser.has_no_context_modifier(result):
                results.append(result)
            else:
                results.append(None)
        return results


    @staticmethod
    def filter_exists(context_result):
        results = []
        for result in context_result:
            if ContextParser.has_no_context_modifier(result) or ContextParser.has_context_modifier(result, ContextParser.exist):
                results.append(result)
            else:
                results.append(None)
        return results

    @staticmethod
    def filter_exists_minus_limited_amount(context_result):
        results = []
        for result in context_result:
            if ContextParser.has_no_context_modifier(result) or ContextParser.has_context_modifier(result, ContextParser.exists_minus_limited_amount):
                results.append(result)
            else:
                results.append(None)
        return results

    @staticmethod
    def pretty_context_output_modifier(context_result):
        results = []
        for result in context_result:
            if not result:
                continue

            for target in result:
                if 'modifier_category' in target:
                    results.append({'target': target['found_phrase'], 'modifier': target['modifier_found_phrase'], 'category': target['modifier_category']})
        return results

    @staticmethod
    def pretty_context_no_modifier(context_result):
        results = []
        for result in context_result:
            if not result:
                continue

            for target in result:
                if not 'modifier_category' in target:
                    results.append({'target:': target['found_phrase']})
        return results

    @staticmethod
    def get_context_sents(doc, context_result):
        results = []
        for idx, sent in enumerate(doc.sents):
            if context_result[idx]:
                results.append(sent)
        return results

    @staticmethod
    def filter_black_list(context_result, context_result_blacklist):
        results = []
        for idx, result in enumerate(context_result):
            if not result:
                results.append(result)

            if not context_result_blacklist[idx]:
                results.append(result)
            else:
                results.append(None)

        return results
