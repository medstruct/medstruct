from json import JSONDecodeError
import requests


class MedstructMeasurementClient(object):

    def __init__(self, url):
        self.url = url

    def get_measurements(self, jsonnlp_context):
        if not jsonnlp_context:
            return None
        try:
            response = requests.post(url=self.url, json=jsonnlp_context)
            print("MeasurementExtractor response ", response.status_code, response.reason)
            return response.json()
        except JSONDecodeError as err:
            print("JSONDecodeError", err, response)
            return None
        except ConnectionError as err:
            print("ConnectionError for MeasurementExtractor", err)
            return None
