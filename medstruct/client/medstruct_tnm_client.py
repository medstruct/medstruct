from json import JSONDecodeError
import requests


class MedstructTnmClient(object):

    def __init__(self, url):
        self.url = url

    def get_tnm(self, jsonnlp_context_measurements):
        if not jsonnlp_context_measurements:
            return None
        try:
            response = requests.post(url=self.url, json=jsonnlp_context_measurements)
            print("MedstructTnmClient response ", response.status_code, response.reason)
            return response.json()
        except JSONDecodeError as err:
            print("JSONDecodeError", err, response)
            return None
