import unittest

from medstruct.component.sectionizer_component import SectionizerComponent


class TestBostonSectionizer(unittest.TestCase):

    def test_sectionizer(self):
        path = "./resources/sections.txt"
        with open(path, 'r') as f:
            data = f.read()
        print(data)
        raw_text = data
        raw_text = SectionizerComponent.get_raw_sections_boston_style(raw_text, ['chest'], ['not-relevant'], ['not-relevant'])


if __name__ == '__main__':
    unittest.main()
