import unittest

from medstruct.xlsx_run import XlsxRun

# old train
t_stage_reports_train = \
    "/Users/sanderputs/Data/Research/TNM-LUNG/reports/t-stage/t-stage-reports-train-latestlabel.xlsx"

t_stage_reports_train_mini = \
    "/data/reports/t-stage/t-stage-reports-train-mini-11.xlsx"

# old val
t_stage_reports_val = \
    "/Users/sanderputs/Data/Research/TNM-LUNG/reports/t-stage/t-stage-reports-val.xlsx"

t_stage_reports_cases = \
    "resources/t-stage-reports-cases-dutch.xlsx"

# only 60
tn_stage_reports_train_nl = \
    "/Users/sanderputs/Data/Research/TNM-LUNG/n/new-n-status 5-12-2019.xlsx"

# new val
tn_stage_reports_validate_nl = \
    "/Users/sanderputs/Data/Research/TNM-LUNG/n/t-stage-reports-val - NIEUWE STAGING MARTIJN.xlsx"



tn_stage_202067plus_nl = \
    "/Users/sanderputs/Data/Research/TNM-LUNG/n/20200106-lung-row67full.xlsx"

tn_stage_20200109_lung_all = \
    "/Users/sanderputs/Data/Research/TNM-LUNG/n/20200109-lung-rad.xlsx"

# new train
tn_stage_20200109_lung_full = \
    "/Users/sanderputs/Data/Research/TNM-LUNG/n/20200109-full.xlsx"

tn_stage_train_final = "/Users/sanderputs/Data/Research/TNM-LUNG/n/final2/results-train-comments.xlsx"

tn_stage_validate_final = "/Users/sanderputs/Data/Research/TNM-LUNG/n/final2/results-val-comments.xlsx"

tn_sbs_mumc = "/Users/sanderputs/Data/Research/MUMC/TNM-LUNG/deep-learning/SBS/sbs-combined.xlsx"
tn_sbs_viecuri = "/Users/sanderputs/Data/Research/MUMC/TNM-LUNG/deep-learning/SBS/results-val-comments-sbs.xlsx"


class TestReportTumor(unittest.TestCase):

    def test_t_stage_reports_train(self):
        XlsxRun().classify(t_stage_reports_train, 'train')

    def test_t_stage_reports_train_mini(self):
        XlsxRun().classify(t_stage_reports_train_mini, 'train-mini')

    def test_t_stage_reports_val(self):
        XlsxRun().classify(t_stage_reports_val, 'validate')

    def test_t_stage_reports_cases(self):
        XlsxRun().classify(t_stage_reports_cases, 'cases')

    def test_tn_stage_reports_train(self):
        XlsxRun().classify(tn_stage_reports_train_nl, 'train-tn')

    def test_tn_stage_reports_validate(self):
        XlsxRun().classify(tn_stage_reports_validate_nl, 'validate-tn')

    def test_tn_stage_reports_train_67(self):
        XlsxRun().classify(tn_stage_202067plus_nl, 'train-tn-67')

    def test_tn_stage_reports_train_20200109all(self):
        XlsxRun().classify(tn_stage_20200109_lung_all, 'train-tn-20200109all')

    def test_tn_stage_reports_train_20200109full(self):
        XlsxRun().classify(tn_stage_20200109_lung_full, 'train-tn-20200109full')

    def test_tn_stage_reports_train_final(self):
        XlsxRun().classify(tn_stage_train_final, 'train-tn-final')

    def test_tn_stage_reports_validate_final(self):
        XlsxRun().classify(tn_stage_validate_final, 'validate-tn-final')

    def test_sbs_mumc(self):
        XlsxRun().classify(tn_sbs_mumc, 'tn_sbs_mumc')

    def test_sbs_viecuri(self):
        XlsxRun().classify(tn_sbs_viecuri, 'tn_sbs_viecuri')

if __name__ == '__main__':
    unittest.main()