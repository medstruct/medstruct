import unittest
import pandas as panda
import os
import yaml

from medstruct.classify.context import classify_context
from medstruct.classify.context.context_stream import ContextStream

source_xlxs = "/Users/sanderputs/Data/Research/TNM-LUNG/reports/lungembeditlabel.xlsx"
results_xlxs = "/Users/sanderputs/Data/Research/TNM-LUNG/reports/lungembeditlabel-results.xlsx"

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
CONFIG_PATH = os.path.join(ROOT_DIR, '../application.yml')


class TestLungEmb(unittest.TestCase):

    def test_context(self):

        column_text = "Verslag!"
        column_id = "Accession number"
        column_label = "Label"

        target_concept = "snomedct:59282003"

        context_stream = ContextStream(CONFIG_PATH)

        df = panda.read_excel(source_xlxs, header=0)

        df['context_modifier'] = df.apply(lambda row: classify_context.context_filter(
            column_id,
            context_stream.get_context(row[column_id], row[column_text], lang="nl"),
                                             target_concept), axis=1)
        # df = df.drop(columns=['source'])
        # return df
        df.to_excel(results_xlxs)

    if __name__ == '__main__':
        unittest.main()