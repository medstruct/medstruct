import unittest
import pandas as pd

from medstruct.xlsx_run import XlsxRun


tnm_base_dir = '/shared-volume/data/tnm/'

m_train_in = tnm_base_dir + 'm_tn-train-results-sbs.xlsx'
m_val_in = tnm_base_dir + 'm_tn-val-results-sbs.xlsx'
m_comb_in = tnm_base_dir + 'm_combined-in-sbs.xlsx'

v_in = tnm_base_dir + 'v_edit_ALLDATA_Selected_PET_CT_and_CT_Thorax_scans-sbs.xlsx'

z_train_in = tnm_base_dir + 'z/training-set-20210407-123750/results-sbs.xlsx'
z_val_in = tnm_base_dir + 'z/validation-set-20210407-123847/results-sbs.xlsx'
z_comb_in = tnm_base_dir + 'z/z_results_comb-sbs.xlsx'

experiments_config_path = "/app/experiments/"

experiments_sum_out = tnm_base_dir + "tnm-summary.xlsx"

# config_full_v = experiments_config_path + "application-full-v.yml"
config_sec = experiments_config_path + "application-sec.yml"
config_full = experiments_config_path + "application-full.yml"
config_thorax_psp_none = experiments_config_path + "application-thorax-psp_50.yml"
config_thorax_psp_50 = experiments_config_path + "application-thorax-psp_50.yml"
config_thorax_psp_60 = experiments_config_path + "application-thorax-psp_60.yml"
config_thorax_psp_70 = experiments_config_path + "application-thorax-psp_70.yml"
config_thorax_psp_80 = experiments_config_path + "application-thorax-psp_80.yml"
config_thorax_psp_90 = experiments_config_path + "application-thorax-psp_90.yml"

dataset_names = {
    "m_train_in" : m_train_in,
    "m_val_in" : m_val_in,
    "m_comb_in" : m_comb_in,
    "v_in" : v_in,
    "z_train_in" : z_train_in,
    "z_val_in" : z_val_in,
    "z_comb_in" : z_comb_in,
}

config_names = {
    "sec" : config_sec,
    # "full" : config_full,
    # "none" : config_thorax_psp_none,
    # "50" : config_thorax_psp_50,
    # "60" : config_thorax_psp_60,
    # "70" : config_thorax_psp_70,
    # "80" : config_thorax_psp_80,
    # "90" : config_thorax_psp_90,
}

class TestShclassifier(unittest.TestCase):

    results =[]
    for dkey in dataset_names:
        for ckey in config_names:
            out_name = dkey + "_" + ckey
            xlsxrun = XlsxRun()
            result = xlsxrun.classify(dataset_names[dkey], out_name, config_names[ckey])
            results.append(result.copy())

    df = pd.DataFrame()
    df = df.append(results, ignore_index=True, sort=False)
    df.to_excel(experiments_sum_out)

if __name__ == '__main__':
    unittest.main()