# docker build . -t putssander/medstruct:2.2
FROM tiangolo/uwsgi-nginx-flask:python3.8
LABEL maintainer="putssander"
LABEL version="2.2"
LABEL description="Medstruct"

# Install the required packages
RUN apt-get update && apt-get install -y \
    build-essential \
    libssl-dev \
    curl && \
    apt-get -q clean -y && rm -rf /var/lib/apt/lists/* && rm -f /var/cache/apt/*.bin

ADD ./README.md  /app/README.md
#RUN pip install git+https://github.com/putssander/Py-JSON-NLP.git
ADD ./setup.py  /app/setup.py
WORKDIR /app/
RUN python setup.py install

COPY . / /app/

ENV PYTHONPATH "${PYTHONPATH}:/app"

ENV LISTEN_PORT 8081

WORKDIR /app
#CMD python medstruct/xlsx_run.py INPUT_XLSX OUTPUT_XLSX
