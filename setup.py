
import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="medstruct-python",
    version='2.0.0',
    python_requires='>=3.8',
    author="Sander Puts, Martijn Nobel",
    author_email="find me on github",
    description="MEDSTRUCT as single standalone module",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/putssander/medstruct",
    packages=setuptools.find_packages(),
    install_requires=[
        'pyjsonnlp>=0.2.12',
        'numpy>=1.14',
        'pyyaml',
        'pandas',
        'scikit-learn',
        'requests',
        'matplotlib',
        'xlrd',
        'openpyxl'
    ],
    # setup_requires=["cython", "numpy>=1.14", "pytest-runner"],
    setup_requires=["cython", "numpy>=1.14", "pytest-runner"],
    classifiers=[
        "Programming Language :: Python :: 3.8",
        "License :: OSI Approved :: CC",
        "Operating System :: OS Independent",
    ],
    # test_suite="tests",
    # tests_require=["pytest", "coverage"]
)
